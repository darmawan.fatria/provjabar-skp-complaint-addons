from openerp.osv import fields, osv
from mx import DateTime
from datetime import datetime, date
from openerp import SUPERUSER_ID


_COMPLAINT_SOURCE = [('pns','PNS'),
                     ('instansi','Instansi'),
                     ('masyarakat','Masyarakat'),
                      ]
_COMPLAINT_STATE = [    ('new','Baru'),
                     ('approved','Diterima'),
                     ('rejected','Ditolak'),
                   ]

class project_perilaku_complaint(osv.Model):
    _name = "project.perilaku.complaint"
    _description='Pelayanan Ketidakpuasan'
    
    def create(self, cr, uid, vals, context=None):
        if vals.get('notes', False) :
            vals['name'] = vals.get('notes', False)
        if vals.get('complaint_source', False) == 'pns' and vals.get('complaint_id_source', False) :
                partner_pool = self.pool.get('res.partner')
                employee_id_source = partner_pool.search(cr, uid, [('nip', '=', vals.get('complaint_id_source', False))], limit=1, context=None)
                if employee_id_source:
                    vals['employee_id_source'] = employee_id_source[0]

        complaint_id = super(project_perilaku_complaint, self).create(cr, uid, vals, context)
        self.update_approval(cr,uid,[complaint_id],context=None)
        return complaint_id
    
    _columns = {
        'employee_id'           : fields.many2one('res.partner', 'Pegawai Yang Dikeluhkan'  ,required=True),
        'state'                 : fields.selection(_COMPLAINT_STATE,'Status',required=True),
        'employee_id_source'    : fields.many2one('res.partner', 'Pemberi Keluhan'  ),
        'complaint_id_source'   : fields.char('NIP/NIK', size=25  ),
        'complaint_name_source' : fields.char('Nama Pemberi Keluhan', size=200  ),
        'name' : fields.char('Keluhan', size=50  ),
        'complaint_phone_source': fields.char('No Telp', size=15  ),
        'complaint_source'      : fields.selection(_COMPLAINT_SOURCE,'Sumber Keluhan',required=True),
        'user_id_atasan'      : fields.many2one('res.users', 'Atasan Langsung'  ),
        'user_id'             : fields.many2one('res.users', 'User ID'  ),
        'notes'                 : fields.text('Keluhan' ),
        'date'                  :fields.date('Tanggal Pemberian Keluhan',),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
        'target_period_year'     : fields.char('Periode Tahun', size=4),
    }
    _defaults = {
        'date': date.today().strftime('%Y-%m-%d'),
        'target_period_year': date.today().strftime('%Y'),
        'target_period_month': date.today().strftime('%m'),
    }
    def update_approval(self, cr, uid, ids, context=None):
        for complaint in self.browse(cr,uid,ids,context=None):
            employee = complaint.employee_id
            self.write(cr, uid, complaint.id, {'user_id':employee.user_id and employee.user_id.id or None,
                                               'user_id_atasan':employee.user_id_atasan and employee.user_id_atasan.user_id and employee.user_id_atasan.user_id.id or None,
                                               }, 
                       context=context) 
    def set_approved(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'approved'}, context=context)
    def set_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'rejected'}, context=context)
    def action_complaint_approve(self, cr, uid, ids, context=None):
        res_update = self.set_approved(cr, uid, ids, context=context)
        perilaku_pool = self.pool.get('project.perilaku')
        for  data_obj in self.browse(cr,uid,ids,context=None):
            user_id = data_obj.user_id.id
            print "data user _id : ",user_id
            perilaku_ids = perilaku_pool.search(cr,uid, [('target_period_year', '=', data_obj.target_period_year),
                                                        ('target_period_month', '=', data_obj.target_period_month),
                                                        ('user_id','=',user_id),('state','!=','done')], context=None)
            print "data perilaku ",perilaku_ids
            complaint_source = data_obj.complaint_source
            perilaku_datas = perilaku_pool.read(cr, uid, perilaku_ids, ['id','user_id','state','realisasi_jumlah_konsumen_pelayanan','realisasi_jumlah_tidakpuas_pelayanan','employee_id','employee_job_type'
                ,'realisasi_jumlah_konsumen_pelayanan_pns','realisasi_jumlah_tidakpuas_pelayanan_pns'
                ,'realisasi_jumlah_konsumen_pelayanan_instansi','realisasi_jumlah_tidakpuas_pelayanan_instansi'
                ,'realisasi_jumlah_konsumen_pelayanan_masyarakat','realisasi_jumlah_tidakpuas_pelayanan_masyarakat'], context=context) #just read . not browse


            for record in perilaku_datas:
                #
                satuan_hitung_obj = self.pool.get('satuan.hitung').browse(cr,uid,record['realisasi_jumlah_konsumen_pelayanan'],context=None)
                if (complaint_source == 'pns') :
                    print "complaint dari sesama pns sebelumnya ", record['realisasi_jumlah_tidakpuas_pelayanan_pns']
                    perilaku_pool.write(cr, SUPERUSER_ID, [record['id']],  {'realisasi_jumlah_tidakpuas_pelayanan_pns':record['realisasi_jumlah_tidakpuas_pelayanan_pns']+1,

                                         }, context)
                if (complaint_source == 'masyarakat') :
                    print "complaint dari masyarakat", record['realisasi_jumlah_tidakpuas_pelayanan_masyarakat']
                    perilaku_pool.write(cr, SUPERUSER_ID, [record['id']],  {'realisasi_jumlah_tidakpuas_pelayanan_masyarakat':record['realisasi_jumlah_tidakpuas_pelayanan_masyarakat']+1,

                                         }, context)
                if (complaint_source == 'instansi') :
                    print "complaint dari instansi", record['realisasi_jumlah_tidakpuas_pelayanan_instansi']
                    perilaku_pool.write(cr, SUPERUSER_ID, [record['id']],  {'realisasi_jumlah_tidakpuas_pelayanan_instansi':record['realisasi_jumlah_tidakpuas_pelayanan_instansi']+1,

                                         }, context)
                if (satuan_hitung_obj and satuan_hitung_obj.complaint_source == complaint_source):
                    print "update the complaint..."
                    perilaku_pool.write(cr, SUPERUSER_ID, [record['id']],  {'realisasi_jumlah_tidakpuas_pelayanan':record['realisasi_jumlah_tidakpuas_pelayanan']+1,
                                         }, context)
        return res_update
    def action_complaint_reject(self, cr, uid, ids, context=None):
        return self.set_rejected(cr, uid, ids, context=context)
project_perilaku_complaint()

