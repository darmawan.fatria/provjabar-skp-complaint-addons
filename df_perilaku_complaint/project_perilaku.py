# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
#from project_perilaku_complaint import _COMPLAINT_SOURCE
#from openerp import SUPERUSER_ID
#from datetime import datetime,timedelta
#import time
#from mx import DateTime


class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'


    _columns = {
        'realisasi_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',domain=[('complaint_source', '!=', False)] ),
        'suggest_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',domain=[('complaint_source', '!=', False)]),
        'appeal_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',domain=[('complaint_source', '!=', False)]),

        'realisasi_jumlah_konsumen_pelayanan_pns'     : fields.integer('Jumlah Pelayanan Terhadap Sesama Pegawai'),
        'realisasi_jumlah_tidakpuas_pelayanan_pns'     : fields.integer('Terhadap Sesama Pegawai'),
        #'realisasi_satuan_jumlah_konsumen_pelayanan_pns': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen'),
        'realisasi_jumlah_konsumen_pelayanan_instansi'     : fields.integer('Jumlah Pelayanan Terhadap Instansi'),
        'realisasi_jumlah_tidakpuas_pelayanan_instansi'     : fields.integer('Terhadap Instansi'),
       # 'realisasi_satuan_jumlah_konsumen_pelayanan_instansi': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen'),
        'realisasi_jumlah_konsumen_pelayanan_masyarakat'     : fields.integer('Jumlah Pelayanan Terhadap Masyarakat'),
        'realisasi_jumlah_tidakpuas_pelayanan_masyarakat'     : fields.integer('Terhadap Masyarakat'),
       # 'realisasi_satuan_jumlah_konsumen_pelayanan_masyarakat': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen'),
    }



project_perilaku()

